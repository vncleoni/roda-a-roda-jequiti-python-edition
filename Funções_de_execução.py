import random
import time
import pygame

#Usei pygame pra tocar musiquinha ;)
pygame.init()
pygame.mixer.music.load('winsong.mp3')

#variáveis globais utilizadas:

global jogadorAtual
global saiu_na_roleta
global Conta_Barbara
global Conta_Carlos
global Conta_Ana
global palavra1
global palavra2
global palavra3
global lista_palavra1
global lista_palavra2
global lista_palavra3
global lista_oculta1
global lista_oculta2
global lista_oculta3
global tentativa
global lista_erradas
global lista_certas
global conta_rodadas
global conta_turno
global temasorteado
global quis_chutar
global vencedor
global palavra_final
global lista_oculta_final
global lista_palavra_final

#Vi necessidade de usar variáveis globais pois estarei atualizando esses valores constantemente, ao mesmo tempo que estarei utilizando eles, atualizados, em mais de uma função

base_de_dados = {'Pedras': ["Quartzo", "Jade", "Jaspe", "Ametista", "Onix"],
                 'Carro': ['Perua', 'Picape', 'Minivan', 'Esportivo', 'Sedan'],
                 'Escola': ["Caneta", "lapiseira", "borracha", "estojo", "lancheira"],
                 'Animais':["Carangueijo", "Elefante", "Galinha", "Cachorro", "Baleia"]}

rodada_final = {'Comida': ["Moqueca", "Acaraje", "Estrogonofe", "Tapioca"],
                'Time de Futebol': ["Athletico Paranaense", "Corinthians", "Fluminense", "XV de Piracicaba"],
                'Politicos': ["Sarney", "Vargas", "Tancredo", "Brizola"]}


lista_palavra_final = []
lista_oculta_final = []
palavra_final = ''
vencedor = ''
Conta_Barbara = 0
Conta_Carlos = 0
Conta_Ana = 0
conta_rodadas = 1
conta_turno = 0
jogadores = ['Ana', 'Bárbara', 'Carlos']
jogadorinicial = jogadores[0]
jogadorAtual = ''
componentes_roleta = ['100', '150', '200', '250', '300', '350', '400', '450', '500', '550', '600', '650', '700', '750',
                      '800', '850', '900', '950', '1000', '1000', 'Passa a Vez', 'Passa a Vez', 'Perdeu Tudo',
                      'Perdeu Tudo']
tentativa = ''
saiu_na_roleta = ''
palavra1 = ""
palavra2 = ''
palavra3 = ''
lista_palavra1 = []
lista_palavra2 = []
lista_palavra3 = []
lista_oculta1 = []
lista_oculta2 = []
lista_oculta3 = []
tentativa = ''
lista_erradas = []
temasorteado = ''
quis_chutar = None


#Funções para a execução do jogo

def JogadorAtual():
    '''Função que retorna a variável de jogador atual'''

    global jogadorAtual

    return jogadorAtual

def temaSorteado():
    '''função para exibição do tema sorteado na tela
    sem entrada->sem saída'''

    global temasorteado

    return temasorteado

def letrasAcertadas():
    '''função para exibição das letras acertadas na tela
    sem entrada->sem saída'''

    global lista_certas

    return lista_certas

def letrasErradas():
    '''função para exibição das letras erradas na tela
        sem entrada->sem saída'''

    global lista_erradas

    return lista_erradas

def mostrar_rodadas():
    '''função para exibição das rodadas jogadas na tela
    sem entrada->sem saída'''

    global conta_rodadas

    print('Rodadas jogadas:' + str(conta_rodadas))
    return conta_rodadas

def mostrar_turnos():
    '''função para exibição dos turnos jogados na tela
    sem entrada->sem saída'''

    global conta_turno

    print('Turnos jogados:' + str(conta_turno))
    return conta_turno

def mostrar_contas():
    '''função para exibição das contas dos jogadores na tela
    sem entrada->sem saída'''

    global Conta_Carlos
    global Conta_Ana
    global Conta_Barbara

    print('Carlos:' + str(Conta_Carlos) + '\n''Bárbara:' + str(Conta_Barbara) + '\n' 'Ana:' + str(Conta_Ana))
    return 'Carlos:' + str(Conta_Carlos) + '\n''Bárbara:' + str(Conta_Barbara) + '\n' 'Ana:' + str(Conta_Ana)

def mostrar_tela_jogo():
    '''Funçáo de exibição de informação do jogo
    sem entrada->sem saída'''

    print('================================================================================================')
    mostrar_contas()
    mostrar_rodadas()
    mostrar_turnos()
    print('')
    print('Jogador atual:', jogadorAtual)
    print('')
    print('Tema:', temaSorteado())
    mostrar_palavras_ocultas()
    print('')
    print('Letras acertadas:', letrasAcertadas())
    print('Letras erradas:', letrasErradas())
    print('')
    print('================================================================================================\n')

def sortear_tema_e_palavras():
    '''Função para sortear o tema e as palavras que serão adivinhadas no jogo
    sem entrada -> tuple'''

    global palavra1
    global palavra2
    global palavra3
    global temasorteado
    global lista_palavra1
    global lista_palavra2
    global lista_palavra3
    global lista_oculta1
    global lista_oculta2
    global lista_oculta3
    global lista_erradas
    global lista_certas

    lista_oculta1 = []
    lista_oculta2 = []
    lista_oculta3 = []
    lista_palavra1 = []
    lista_palavra2 = []
    lista_palavra3 = []
    lista_certas = []
    lista_erradas = []
    # vou ler por arquivo de texto a base de dados
    chaves = base_de_dados.keys()
    listola_de_chaves = list(chaves)
    random.shuffle(listola_de_chaves)
    # para temas

    temasorteado = random.choice(listola_de_chaves)
    palavras_possiveis = base_de_dados[temasorteado]
    escolhidas = random.sample(palavras_possiveis, k=3)

    palavra1 = escolhidas[0]
    palavra2 = escolhidas[1]
    palavra3 = escolhidas[2]
    lista_palavra1 = list(palavra1.strip().upper())
    lista_palavra2 = list(palavra2.strip().upper())
    lista_palavra3 = list(palavra3.strip().upper())
    return palavra1, palavra2, palavra3

def ocultar_palavras():
    '''Função que  cria uma lista de _ com o mesmo tamanho das palavras sorteadas
    sem entrada->sem saída'''

    global lista_palavra1
    global lista_palavra2
    global lista_palavra3
    global lista_oculta1
    global lista_oculta2
    global lista_oculta3
    tamanholista1 = len(lista_palavra1)
    tamanholista2 = len(lista_palavra2)
    tamanholista3 = len(lista_palavra3)
    i = 0
    j = 0
    n = 0

    while i < tamanholista1:
        lista_oculta1.append('_')
        i += 1
    while j < tamanholista2:
        lista_oculta2.append('_')
        j += 1
    while n < tamanholista3:
        lista_oculta3.append('_')
        n += 1

def decretar_vencedor():
    '''Função que retorna o jogador vencedor no final da partida
    sem entrada->string'''

    global Conta_Carlos
    global Conta_Ana
    global Conta_Barbara
    global vencedor
    global jogadorAtual

    parcial = [Conta_Barbara, Conta_Ana, Conta_Carlos]

    if parcial[1] == parcial[2] and parcial[1] == parcial[0] and parcial[2] == parcial[0]:
        return 'bizarramente deu empate', quit()
    parcial.sort()

    if parcial[2] == Conta_Barbara:
        vencedor = jogadores[1]
        jogadorAtual = jogadores[1]
        return 'O vencedor é Bárbara'

    elif parcial[2] == Conta_Ana:
        vencedor = jogadores[0]
        jogadorAtual = jogadores[0]
        return 'O vencedor é Ana'

    elif parcial[2] == Conta_Carlos:
        vencedor = jogadores[2]
        jogadorAtual = jogadores[2]
        return 'O vencedor é Carlos'

def rodada_bonus():
    '''Função que inicia a rodada bônus
    sem entrada->sem saída'''

    print('Silvio Santos: Hah Haaaaaai!!!!!')
    time.sleep(2)
    print('Silvio Santos: Vamos agora pra rodadaaaaa boooooonuuuuus!!!!!')
    time.sleep(2)
    print('Parabéns,', JogadorAtual())
    time.sleep(2)
    print(
        'Silvio Santos: Vai aparecer uma palavra na tela, você vai me dizer uma vogal e cinco consoantes que você acha que tem a palavra, beleza?')
    time.sleep(2)
    print('Silvio Santos: Hah Haaaaaai!!!!!')
    time.sleep(2)
    jogo_final()

def jogo_final():
    '''Função que puxa as funções para o jogo da rodada final
    sem entrada -> sem saída'''

    global temasorteado
    print('Silvio Santos: Vamos agora para a rodada bônus!!')
    sortear_tema_e_palavra_final()
    print('Silvio Santos: Palavras na tela!!')
    time.sleep(2)
    ocultar_palavra_final()
    mostrar_tela_jogo_final()
    print("Silvio Santos: O tema é", temasorteado)
    time.sleep(2)
    gameplay_final()

def gameplay_final():
    '''Função que executa a função da tentativa de acertar uma letra das palavras ocultas chamando outras funções
    sem entrada -> sem saída'''

    # só pode ser chamada depois que rodar_roleta e sortear_tema_e_palavras forem chamadas

    global palavra_final

    palavra_final = palavra_final.upper()
    tratar_tentativa_final()

def tratar_tentativa_final():
    '''Função que executa a função da tentativa de acertar uma letra das palavras ocultas chamando outras funções
    sem entrada -> sem saída'''

    global palavra_final
    global lista_oculta_final
    global tentativa
    global lista_palavra_final

    tentativa = input('Silvio Santos: Valendo o dobro do que você ganhou até agora; cinco consoantes e uma vogal?\n Digite assim:(Consoante1Consoante2Consoante3Consoante4Consoante5Vogal)\n Exemplo: cdfgha \n')

    tentativas = list(tentativa.strip().upper())

    if tentativa.strip().isalpha() == False:
        print('Silvio Santos: Digite  cinco consoantes e uma vogal\n Assim:(Consoante1Consoante2Consoante3Consoante4Consoante5Vogal)\n Exemplo: cdfgha \n')
        time.sleep(2)
        tratar_tentativa_final()

    if len(tentativas) != 6:
        print('Silvio Santos: Digite  cinco consoantes e uma vogal\n Assim:(Consoante1Consoante2Consoante3Consoante4Consoante5Vogal)\n Exemplo: cdfgha \n')
        time.sleep(2)
        tratar_tentativa_final()

    #metodo para verificar se as 5 primeiras letras são consoantes
    i = 0
    taerradoconsoante = False
    temletrasiguais = False
    while i < len(tentativas) - 2:
        if tentativas[i] not in 'bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ':
            taerradoconsoante = True
        i += 1

    j = 0
    w = 0
    while j < len(tentativas) - 2:
        if tentativas.count(tentativas[j]) != 1:
            temletrasiguais = True
        j += 1

    if temletrasiguais == True:
        print('Silvio Santos: Digite  cinco consoantes e uma vogal, sem repetir as consoantes\n Assim:(Consoante1Consoante2Consoante3Consoante4Consoante5Vogal)\n Exemplo: cdfgha \n')
        time.sleep(2)
        tratar_tentativa_final()

    if taerradoconsoante == True:
        print('Silvio Santos: Digite  cinco consoantes e uma vogal\n Assim:(Consoante1Consoante2Consoante3Consoante4Consoante5Vogal)\n Exemplo: cdfgha \n')
        time.sleep(2)
        tratar_tentativa_final()

    if tentativas[len(tentativas)-1] not in "AEIOUaeiou":
        print('Silvio Santos: Digite  cinco consoantes e uma vogal\n Assim:(ConsoanteConsoanteConsoanteConsoanteConsoanteVogal)\n Exemplo: cdfgha \n')
        time.sleep(2)
        tratar_tentativa_final()



    if tentativas[0] in lista_palavra_final or tentativas[1] in lista_palavra_final or tentativas[2] in lista_palavra_final or tentativas[3] in lista_palavra_final or tentativas[4] in lista_palavra_final or tentativas[5] in lista_palavra_final:
            print('Silvio Santos: Acertou!!!!')
            time.sleep(2)

            if tentativas[0] in palavra_final:
                posicao = 0
                for g in palavra_final:
                    if g == tentativas[0]:
                        lista_oculta_final[posicao] = tentativas[0]
                    posicao += 1

            if tentativas[1] in palavra_final:
                posicao = 0
                for g in palavra_final:
                    if g == tentativas[1]:
                        lista_oculta_final[posicao] = tentativas[1]
                    posicao += 1

            if tentativas[2] in palavra_final:
                posicao = 0
                for g in palavra_final:
                    if g == tentativas[2]:
                        lista_oculta_final[posicao] = tentativas[2]
                    posicao += 1

            if tentativas[3] in palavra_final:
                posicao = 0
                for g in palavra_final:
                    if g == tentativas[3]:
                        lista_oculta_final[posicao] = tentativas[3]
                    posicao += 1

            if tentativas[4] in palavra_final:
                posicao = 0
                for g in palavra_final:
                    if g == tentativas[4]:
                        lista_oculta_final[posicao] = tentativas[4]
                    posicao += 1

            if tentativas[5] in palavra_final:
                posicao = 0
                for g in palavra_final:
                    if g == tentativas[5]:
                        lista_oculta_final[posicao] = tentativas[5]
                    posicao += 1

            mostrar_tela_jogo_final()
            tentativa_chute_final()

    elif tentativas[0] not in lista_palavra_final and tentativas[1] not in lista_palavra_final and tentativas[2] not in lista_palavra_final and tentativas[3] not in lista_palavra_final and tentativas[4] not in lista_palavra_final and tentativas[5] not in lista_palavra_final:
        print('Silvio Santos: Ihhh erroooouuuu tuuuuudoooooo!!!!')
        time.sleep(2)
        decretar_fim_jogo_pos_rodada_final()

def tentativa_chute_final():
    '''Função de triagem do chute da palavra da rodada bônus
    sem entrada -> sem saída'''
    global palavra_final
    global lista_palavra_final
    global lista_oculta_final

    print("Silvio Santos: Agora a partir do que você acertou, tente adivinhar a palavra")
    time.sleep(2)
    chute = input('Escreva a palavra assim: palavra')
    chute = chute.upper()
    if chute == palavra_final :
        print('\nSilvio Santos: Acertoooooou!!!\n', 'Conseguiu duplicar seu dinheiro,', JogadorAtual(),"!!!")
        lista_oculta_final = lista_palavra_final
        mostrar_tela_jogo_final()
        duplicar_valor_contas()
        decretar_fim_jogo_pos_rodada_final()

    else:
        print('\nSilvio Santos: Erroooooou!!! Hah haaaaaaaaai!!!\n', 'Perdeu a chance de duplicar seu dinheiro,', JogadorAtual(), "!!!")
        decretar_fim_jogo_pos_rodada_final()

def duplicar_valor_contas():
    '''Função que duplica o valor da conta do jogador atual
    sem entrada -> int'''

    global Conta_Carlos
    global Conta_Ana
    global Conta_Barbara

    JogadorAtual()
    if JogadorAtual() == 'Carlos':
        Conta_Carlos = Conta_Carlos * 2

        return Conta_Carlos

    if JogadorAtual() == 'Bárbara':
        Conta_Barbara = Conta_Barbara * 2

        return Conta_Barbara

    if JogadorAtual() == 'Ana':
        Conta_Ana = Conta_Ana * 2

        return Conta_Ana

def mostrar_palavra_oculta_final():
    '''Função que  pega a palavra final, transforma em lista de letras, cria uma lista de _ com o mesmo tamanho
    sem entrada -> sem saída'''

    global lista_oculta_final

    print("Palavra: " + ' '.join(lista_oculta_final), sep='\n')

def mostrar_tela_jogo_final():
    '''Funçáo de exibição de informação do jogo na rodada Bônus
    sem entrada -> sem saída'''

    print('================================================================================================')
    mostrar_contas()
    print('Jogador atual:', jogadorAtual)
    print('')
    print('Tema:', temaSorteado())
    mostrar_palavra_oculta_final()
    print('')
    print('================================================================================================\n')

def ocultar_palavra_final():
    '''Função que pega a palavra final, transforma em lista de letras e retorna uma lista de _ com o mesmo tamanho
    sem entrada -> list'''

    global lista_oculta_final
    global lista_palavra_final

    tamanholistafinal = len(lista_palavra_final)

    i = 0

    while i < tamanholistafinal:

        if lista_palavra_final[i].isalpha() == True:
            lista_oculta_final.append('_')
        else:
            lista_oculta_final.append(lista_palavra_final[i])
        i += 1

    return lista_oculta_final

def sortear_tema_e_palavra_final():
    '''Função que sorteia o tema e a palavra da rodada bônus
    sem entrada -> sem saída'''

    global temasorteado
    global palavra_final
    global lista_erradas
    global lista_certas
    global lista_palavra_final

    lista_certas = []
    lista_erradas = []
    # vou ler por arquivo de texto a base de dados
    chaves = rodada_final.keys()
    listola_de_chaves = list(chaves)
    random.shuffle(listola_de_chaves)
    # para temas

    temasorteado = random.choice(listola_de_chaves)
    palavras_possiveis = rodada_final[temasorteado]
    escolhidas = random.sample(palavras_possiveis, k=2)

    palavra_final = escolhidas[0]
    lista_palavra_final = list(palavra_final.upper())
    return palavra_final

def decretar_fim_jogo_pos_rodada_final():
    '''Função que decreta o final do jogo pós rodada final
    sem entrada->sem saída'''

    print('================================================================================================')
    mostrar_contas()
    mostrar_rodadas()
    mostrar_turnos()
    print('')
    print('                                    ', decretar_vencedor())
    print('')
    print('                                    ','Obrigado por jogar!')
    print('')
    print('================================================================================================\n')
    pygame.mixer.music.load('winsong.mp3')
    pygame.mixer.music.play()
    pygame.event.wait()
    quit()

def decretar_fim_jogo():
    ''''Função que decreta o final do jogo e inicia a rodada bônus
    sem entrada-> sem saída'''

    decretar_vencedor()
    print('================================================================================================')
    mostrar_contas()
    mostrar_rodadas()
    mostrar_turnos()
    print('')
    print('                                    ', decretar_vencedor())
    print('')
    print('================================================================================================\n')
    time.sleep(5)
    rodada_bonus()

def checar_pode_chutar():
    '''Função que verifica se o jogador pode tentar adivinhar quais as 3 palavras ou não
    sem entrada-> bool'''

    global lista_palavra1
    global lista_palavra2
    global lista_palavra3
    global lista_oculta1
    global lista_oculta2
    global lista_oculta3

    contador1 = 0
    contador2 = 0
    contador3 = 0
    if lista_palavra1 != lista_oculta1:
        i = 0
        for letra in lista_oculta1:
            if letra != lista_palavra1[i]:
                contador1 += 1
            i += 1
    if lista_palavra2 != lista_oculta2:
        i = 0
        for letra in lista_oculta2:
            if letra != lista_palavra2[i]:
                contador2 += 1
            i += 1
    if lista_palavra3 != lista_oculta3:
        i = 0
        for letra in lista_oculta3:
            if letra != lista_palavra3[i]:
                contador3 += 1
            i += 1
    if contador1 + contador2 + contador3 <= 3:
        return True
    else:
        return False

def checar_acabou_jogo():
    '''Função que verifica se o jogo já acabou ou não
    sem entrada->bool'''

    global conta_rodadas

    if conta_rodadas > 3:
        return True
    else:
        return False

def checar_acabou_rodada():
    '''Função que checa se a rodada já acabou e pode iniciar uma nova
    sem entrada->bool'''

    global conta_rodadas
    global lista_palavra1
    global lista_palavra2
    global lista_palavra3
    global lista_oculta1
    global lista_oculta2
    global lista_oculta3

    if conta_rodadas > 3:
        decretar_fim_jogo()
    else:
        if lista_palavra1 == lista_oculta1 and lista_palavra2 == lista_oculta2 and lista_palavra3 == lista_oculta3:
            '''rodada acabou'''

            conta_rodadas += 1
            return True
        else:
            return False

def apresentandojugadores():
    '''Função que introduz o jogo
    sem entrada-> sem saída'''

    print("Silvio Santos: Mah oeeeeeeee!!")
    time.sleep(2)
    print("Silvio Santos: Maquemquer diêroooo?!")
    time.sleep(2)

    print("Silvio Santos: Então hoje vão jogar:", ', '.join(jogadores[:len(jogadores)]), ".")
    time.sleep(2)

def tratar_resultado():
    '''Função de tiragem que interpreta e trata o resultado da roleta
    sem entrada->sem saída'''
    global jogadorAtual
    global Conta_Barbara
    global Conta_Ana
    global Conta_Carlos
    global saiu_na_roleta
    if saiu_na_roleta.isnumeric() != True:

        if saiu_na_roleta == 'Perdeu Tudo':
            time.sleep(2)
            print("Silvio Santos: Ihhhhhh Perdeu tuuuuuuudooooo!!!!!")
            if jogadorAtual == '':
                jogadorAtual = jogadores[0]
                pass
            if jogadorAtual == 'Carlos':
                Conta_Carlos = 0

            if jogadorAtual == 'Bárbara':
                Conta_Barbara = 0

            if jogadorAtual == 'Ana':
                Conta_Ana = 0

            troca_jogador()
            mostrar_tela_jogo()
            jogada()

        if saiu_na_roleta == 'Passa a Vez':
            troca_jogador()
            mostrar_tela_jogo()
            checar_acabou_rodada()
            jogada()

    else:
        pass

def rodar_roleta():
    '''Função que faz a roleta girar (sorteio)
    sem entrada->str'''

    global saiu_na_roleta

    random.shuffle(componentes_roleta)
    saiu_na_roleta = random.choice(componentes_roleta)
    print('Silvio Santos: Saiu um', saiu_na_roleta, "na roleta!\n")

    return saiu_na_roleta

def mostrar_palavras_ocultas():
    '''Função que exibe as palavras ocultas na tela
    sem entrada -> sem saída'''

    print("Palavra 1 " + ' '.join(lista_oculta1), "Palavra 2 " + ' '.join(lista_oculta2),
          "Palavra 3 " + ' '.join(lista_oculta3), sep='\n')
    # pega a palavra, transforma em lista de letras, cria uma lista de _ com o mesmo tamanho

def atribuir_ponto():
    '''Função que atribui os pontos da roleta à conta do jogador atual
    sem entrada -> int'''

    global Conta_Carlos
    global Conta_Ana
    global Conta_Barbara
    global jogadorAtual
    global saiu_na_roleta

    if saiu_na_roleta.isdigit() == True:
        if jogadorAtual == '':
            jogadorAtual = jogadores[0]
            pass
        if jogadorAtual == 'Carlos':
            Conta_Carlos += int(saiu_na_roleta)

            return Conta_Carlos

        if jogadorAtual == 'Bárbara':
            Conta_Barbara += int(saiu_na_roleta)

            return Conta_Barbara

        if jogadorAtual == 'Ana':
            Conta_Ana += int(saiu_na_roleta)

            return Conta_Ana

def troca_jogador():
    '''Função que troca o jogador atual
    sem entrada->str'''

    global jogadorAtual
    global conta_turno

    conta_turno = 0
    if jogadorAtual == '':
        jogadorAtual = jogadores[0]
        pass

    if jogadorAtual == jogadores[0]:
        jogadorAtual = jogadores[1]
        print('Silvio Santos: Passamos a vez agora para', jogadores[1], '!')
        time.sleep(2)
        return jogadorAtual

    if jogadorAtual == jogadores[1]:
        jogadorAtual = jogadores[2]
        print('Silvio Santos: Passamos a vez agora para', jogadores[2], '!')
        time.sleep(2)
        return jogadorAtual

    if jogadorAtual == jogadores[2]:
        jogadorAtual = jogadores[0]
        print('Silvio Santos: Passamos a vez agora para', jogadores[0], '!')
        time.sleep(2)
        return jogadorAtual

def jogador_inicial():
    '''Função auxiliar que define o jogador inicial
    sem entrada-> sem saída'''

    global jogadorAtual

    jogadorAtual = jogadorinicial

def jogada():
    '''Função que executa a tentativa de acertar uma letra das palavras ocultas chamando outras funções
    sem entrada -> sem saída'''

    global quis_chutar

    checar_pode_chutar()
    if checar_pode_chutar() == True:
        tentativa_chute()
        if quis_chutar != False:
            if checar_acabou_rodada() == False:
                gameplay()
            else:
                jogo()


        else:
            pode_chutar_ou_nao()

    else:
        if checar_acabou_rodada() == False:
            gameplay()
        else:
            jogo()

def pode_chutar_ou_nao():
    '''Função auxiliar de triagem para quando o usuário digita não para o chute das palavras e não entrar em um loop infinito de perguntas
    sem entrada -> sem saída'''

    global quis_chutar

    checar_pode_chutar()
    if checar_pode_chutar() == True:
        if quis_chutar != False:
            gameplay()
        else:
            quis_chutar = None
            gameplay()

def gameplay():
    '''Função auxiliar de triagem que executa a jogada (precisei recortar a jogada em 3 funções para acessar algumas etapas especificas em alguns momentos do jogo)
    sem entrada -> sem saida'''
    # só pode ser chamada depois que rodar_roleta e sortear_tema_e_palavras forem chamadas

    global palavra1
    global palavra2
    global palavra3

    palavra1 = palavra1.strip().upper()
    palavra2 = palavra2.strip().upper()
    palavra3 = palavra3.strip().upper()
    input("Pressione Enter para rodar a roleta")
    rodar_roleta()
    time.sleep(2)
    tratar_tentativa()

def tratar_tentativa():
    '''Função de triagem que trata a tentativa de acertar uma letra nas 3 palavras escondidas
    sem entrada-> sem saída'''

    global palavra1
    global palavra2
    global palavra3
    global lista_oculta1
    global lista_oculta2
    global lista_oculta3
    global tentativa
    global lista_erradas
    global lista_certas
    global lista_palavra1
    global lista_palavra2
    global lista_palavra3
    global conta_turno

    if saiu_na_roleta.isdigit() == True:
        tentativa = input('Silvio Santos: Valendo {} Reais, uma letra? \n'.format(saiu_na_roleta))

        tentativa = tentativa.strip().upper()

        if tentativa.isalpha() == False:
            print('Silvio Santos: Digite uma letra.')
            time.sleep(2)
            tratar_tentativa()

        if len(tentativa) != 1:
            print('Silvio Santos: Digite apenas uma letra, amigo.')
            time.sleep(2)
            tratar_tentativa()

        if tentativa in lista_certas:
            print('Silvio Santos: Essa letra já foi, escolhe outra!')
            time.sleep(2)
            tratar_tentativa()

        if tentativa in lista_erradas:
            print('Silvio Santos: Essa letra já foi, errada inclusive, escolhe outra!')
            time.sleep(2)
            tratar_tentativa()

        if tentativa in lista_palavra1 or tentativa in lista_palavra2 or tentativa in lista_palavra3:
            print('Acertou!!!!')
            time.sleep(2)
            atribuir_ponto()
            lista_certas.append(tentativa)
            if tentativa in palavra1:
                posicao = 0
                for g in palavra1:
                    if g == tentativa:
                        lista_oculta1[posicao] = tentativa
                    posicao += 1

            if tentativa in palavra2:

                posicao = 0
                for g in palavra2:
                    if g == tentativa:
                        lista_oculta2[posicao] = tentativa
                    posicao += 1

            if tentativa in palavra3:

                posicao = 0
                for g in palavra3:
                    if g == tentativa:
                        lista_oculta3[posicao] = tentativa
                    posicao += 1
            conta_turno += 1
            mostrar_tela_jogo()
            if checar_acabou_rodada() == False:
                jogada()
            else:
                jogo()

        elif tentativa not in lista_palavra1 and tentativa not in lista_palavra2 and tentativa not in lista_palavra3:
            conta_turno += 1
            print('Ihhh Erroooouuuu!!!!')
            lista_erradas.append(tentativa)
            time.sleep(2)
            mostrar_tela_jogo()
            troca_jogador()
            jogada()
        else:

            tratar_resultado()

    else:
        tratar_resultado()

def tentativa_chute():
    '''Função de triagem que trata a tentativa de acertar as 3 palavras escondidas
    sem entrada-> sem saída'''

    global palavra1
    global palavra2
    global palavra3
    global tentativa
    global lista_palavra1
    global lista_palavra2
    global lista_palavra3
    global conta_rodadas
    global lista_oculta1
    global lista_oculta2
    global lista_oculta3
    global quis_chutar

    if checar_pode_chutar() == True:
        time.sleep(2)
        resposta = input('Silvio Santos: Quer tentar adivinhar as palavras? (SIM ou NAO)')
        if resposta.upper() == "NAO":
            quis_chutar = False
            pode_chutar_ou_nao()
        if resposta.upper() == "SIM":
            chute = input('Escreva as palavras assim: palavra1 palavra2 palavra3')
            chutes = chute.strip().upper().split(' ')
            if chutes[0] == palavra1 and chutes[1] == palavra2 and chutes[2] == palavra3:
                print('\nSilvio Santos: Acertoooooou!!!\n')
                conta_rodadas += 1
                lista_oculta1 = lista_palavra1
                lista_oculta2 = lista_palavra2
                lista_oculta3 = lista_palavra3
                mostrar_tela_jogo()
                jogo()

            else:
                print('\nSilvio Santos: Erroooooou!!! Hah haaaaaaaaai!!!\n')
                troca_jogador()
                jogada()


        else:
            print('Responda com SIM ou NAO, amigo')
            tentativa_chute()

def jogo():
    '''Função de triagem para o início do jogo e das demais rodadas
    sem entrada -> sem saída'''

    global conta_rodadas

    checar_acabou_jogo()
    if checar_acabou_jogo() == False:

        print('Vamos agora para a rodada', conta_rodadas)
        sortear_tema_e_palavras()
        print('Silvio Santos: palavras na tela!')
        time.sleep(2)
        ocultar_palavras()
        mostrar_tela_jogo()
        print("Silvio Santos: O tema é", temaSorteado())
        time.sleep(2)
        print("Silvio Santos: Roda a roleta aí pra gente", JogadorAtual(), "!!")
        time.sleep(0.5)
        jogada()

def intro():
    '''Função de triagem que apresenta e inicia o jogo
    sem entrada -> sem saída'''
    pygame.mixer.music.load('tema.mp3')
    pygame.mixer.music.play()
    pygame.event.wait()
    print("Bom dia Brasil!!! Está começando agora, o Roda a Roda Jequiti!!!!")
    time.sleep(2)
    print("Hoje, excepcionalmente, o programa será apresentado por ele! SIIILVIO SANTOOOOOOOS!!!!!!!")
    time.sleep(2)
    apresentandojugadores()
    print('Silvio Santos: O primeiro a jogar será:', JogadorAtual())
    time.sleep(2)

