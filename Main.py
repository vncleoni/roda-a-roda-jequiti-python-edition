
from Funções_de_execução import *

def main():
    '''função main que vai automaticamente executar o jogo'''
    jogador_inicial()
    intro()
    jogo()

if __name__ == "__main__":
    main()
